# Authentipay

Satispay [HTTP Signature](https://tools.ietf.org/html/draft-cavage-http-signatures-05) [assessment](https://www.notion.so/Satispay-Signature-implementation-e4c52c000fb54d7ab1f1d7c8057da54c) implementation.

## Description
With this application you can call the 
```https://staging.authservices.satispay.com/wally-services/protocol/tests/signature``` 
endpoint including the required HTTP signature.

The user has the ability to choose:
- The **HTTP method** of the call (```GET```/```POST```/```PUT```/```PATCH```/```DELETE```).

- The **HTTP message's body**. The body is provided with a command parameter, it can be either a string or the path of a file containing the body content (UTF-8 encoded).

```
Usage: <main class> [-hV] [-b=body] [-f=file] method
      method        The HTTP method you want to use: GET/POST/PUT/PATCH/DELETE. (Default is GET)
  -b, --body=body   A string representing the request's body. The -f|--file
                      option overwrite this.
  -f, --file=file   The path of a JSON file containing the request's body.
  -h, --help        Show this help message and exit.
  -V, --version     Print version information and exit.
```

The HTTP Signature is built with the following HTTP headers: **Host**, **Date**, **Digest** (if it's not a ```GET``` request).

## Run with Gradle

1. Clone or download this repository and open a new terminal window inside it.

2. Run it:
```
$ ./gradlew run --args="post --body='{"hello": "world"}'"
```

## Run as a command

1. Clone or download this repository and open a new terminal window inside it.
2. Create the executable first:
```
$ ./gradlew distZip
```

3. Unzip the resulting zip file:
```
$ unzip ./build/distributions/authentipay.zip 
```
This command will produce a folder called ```authentipay``` inside the project's directory, containing the executable and its dependencies.

4. Run the executable:
```
$ ./authentipay/bin/authentipay post --body='{"hello": "world"}'
```
