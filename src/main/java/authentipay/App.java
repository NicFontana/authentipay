package authentipay;

import authentipay.cli.AuthentipayCommand;
import authentipay.cli.RequestMethodConverter;
import authentipay.http.RequestMethod;
import picocli.CommandLine;

/**
 * Application entry point.
 */
public class App {
    public static void main(String[] args) {
        CommandLine commandLine = new CommandLine(new AuthentipayCommand());
        commandLine.registerConverter(RequestMethod.class, new RequestMethodConverter());
        int exitCode = commandLine.execute(args);

        System.exit(exitCode);
    }
}
