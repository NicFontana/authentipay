package authentipay.signature;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;

/**
 * Class that sign data with RSA.
 */
public class RSASigner {

    /**
     * The signature algorithm string identifier as specified here:
     * https://docs.oracle.com/javase/8/docs/technotes/guides/security/StandardNames.html#Signature
     */
    private String signatureAlgorithm;

    /**
     * The private key's filename.
     * If this is specified it takes precedence over the privateKeyBytes.
     */
    private String privateKeyFilename;

    /**
     * The content (as byte[]) of the private key file.
     */
    private byte[] privateKeyBytes;

    /**
     * Builder class used to create the RSASigner object.
     */
    public static class Builder {
        private String signatureAlgorithm;
        private String privateKeyFilename;
        private byte[] privateKeyBytes;

        public Builder setSignatureAlgorithm(String signatureAlgorithm) {
            this.signatureAlgorithm = signatureAlgorithm;
            return this;
        }

        public Builder setPrivateKeyFilename(String privateKeyFilename) {
            this.privateKeyFilename = privateKeyFilename;
            return this;
        }

        public Builder setPrivateKeyBytes(byte[] privateKeyBytes) {
            this.privateKeyBytes = privateKeyBytes;
            return this;
        }

        public RSASigner build() {
            return new RSASigner(this);
        }
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    private RSASigner() {
    }

    private RSASigner(Builder builder) {
        signatureAlgorithm = builder.signatureAlgorithm;
        privateKeyFilename = builder.privateKeyFilename;
        privateKeyBytes = builder.privateKeyBytes;
    }

    /**
     * @param data: the data to be signed.
     * 
     * @return the signature's value in byte[].
     */
    public byte[] sign(byte[] data) {
        PrivateKey privateKey = privateKeyFilename != null ? getPrivateKey(privateKeyFilename)
                : getPrivateKey(privateKeyBytes);
        return signData(privateKey, signatureAlgorithm, data);
    }

    /**
     * @param filename: The private key's filename.
     * 
     * @return the PrivateKey object obtained from the private key file.
     */
    private PrivateKey getPrivateKey(String privateFilename) {
        final int bufferLen = 4 * 1024; // 4KB
        byte[] buffer = new byte[bufferLen];

        try {
            InputStream resourceAsStream = RSASigner.class.getClassLoader().getResourceAsStream(privateFilename);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            int readedCount = 0;
            while ((readedCount = resourceAsStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, readedCount);
            }
            byte[] privateKeyBytes = outputStream.toByteArray();
            outputStream.close();
            resourceAsStream.close();

            return getPrivateKey(privateKeyBytes);
        } catch (IOException e) {
            e.printStackTrace(System.err);
            System.exit(-1);
        }
        return null;
    }
    
    /**
     * @param privateKeyBytes: The content (as byte[]) of the private key file.
     * 
     * @return the PrivateKey object obtained from the private key bytes.
     */
    private PrivateKey getPrivateKey(byte[] privateKeyBytes) {
        try {
            String privateKeyString = new String(privateKeyBytes, StandardCharsets.UTF_8);
            privateKeyString = privateKeyString.replace("-----BEGIN PRIVATE KEY-----", "");
            privateKeyString = privateKeyString.replace("-----END PRIVATE KEY-----", "");
            privateKeyString = privateKeyString.replace("\n", "");

            PKCS8EncodedKeySpec pkcs8 = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKeyString.getBytes()));
            return KeyFactory.getInstance("RSA").generatePrivate(pkcs8);
        } catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
            e.printStackTrace(System.err);
            System.exit(-1);
        }
        return null;
    }

    /**
     * @param privateKey: the PrivateKey object that must be used to sign the data.
     * @param signatureAlgorithm: The signature algorithm string identifier as specified here: 
     * https://docs.oracle.com/javase/8/docs/technotes/guides/security/StandardNames.html#Signature* 
     * @param data: tha data to be signed.
     * 
     * @return the signed data as byte[].
     */
    private byte[] signData(PrivateKey privateKey, String signatureAlgorithm, byte[] data) {
        try {
            Signature signature = Signature.getInstance(signatureAlgorithm);
            signature.initSign(privateKey);
            signature.update(data);

            return signature.sign();
        } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {
            e.printStackTrace(System.err);
            System.exit(-1);  
        }

        return null;
    }
}
