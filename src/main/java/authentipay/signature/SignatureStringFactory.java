package authentipay.signature;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import authentipay.http.RequestMethod;
import okhttp3.HttpUrl;
import okhttp3.internal.http2.Header;

/**
 * Utility class to generate the Signature String
 */
public class SignatureStringFactory {

    /**
     * @param requestMethod: the HTTP request's method.
     * @param url: the HTTP request's URL.
     * @param headers: a list of RequestHeader objects that must be used to build the Signature String.
     * https://tools.ietf.org/html/draft-cavage-http-signatures-05#section-2.3
     * 
     * @return the Signature String that will be signed with the private key.
     */
    public static String make(RequestMethod requestMethod, HttpUrl url, List<Header> headers) {
        ArrayList<String> components = new ArrayList<String>();

        String requestTargetValue = url.uri().getPath();
        if (url.uri().getQuery() != null) {
            requestTargetValue += "?" + url.uri().getQuery();
        }

        components.add(
            String.join(" ", "(request-target):", requestMethod.toString().toLowerCase(), requestTargetValue)
            );
        components.addAll(
            headers.stream().map((header) -> formatHeaderForSignatureString(header)).collect(Collectors.toList())
            );
        
        return String.join("\n", components);
    }

    /**
     * https://tools.ietf.org/html/draft-cavage-http-signatures-05#section-2.3 
     * 
     * @return: the header's string representation used to build the Signature String.
     */
    private static String formatHeaderForSignatureString(Header header) {
        StringBuilder sb = new StringBuilder();
        sb.append(header.name.utf8().toLowerCase())
            .append(": ")
            .append(header.value.utf8().trim());

        return sb.toString();
    }
}
