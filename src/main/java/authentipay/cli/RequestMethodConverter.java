package authentipay.cli;

import authentipay.http.RequestMethod;
import picocli.CommandLine.ITypeConverter;
import picocli.CommandLine.TypeConversionException;

/**
 * PicoCli type converter for the authentipay.http.RequestMethod class.
 * https://picocli.info/#_custom_type_converters 
 */
public class RequestMethodConverter implements ITypeConverter<RequestMethod> {
    
    public RequestMethod convert(String value) throws Exception {
        switch (value.toLowerCase()) {
            case "get":
                return RequestMethod.GET;
            case "post":
                return RequestMethod.POST;
            case "put":
                return RequestMethod.PUT;
            case "patch":
                return RequestMethod.PATCH;
            case "delete":
                return RequestMethod.DELETE;
            default:
                throw new TypeConversionException(value + " is not a valid HTTP method, choose between: GET/POST/PUT/PATCH/DELETE.");
        }
    }
}
