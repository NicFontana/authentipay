package authentipay.cli;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.io.File;
import java.util.concurrent.Callable;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import authentipay.Constants;
import authentipay.http.RequestFactory;
import authentipay.http.RequestMethod;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.Option;
import picocli.CommandLine.Command;

/**
 * The PicoCli command that takes care of command interpretation.
 * https://picocli.info/#_introduction
 */
@Command(mixinStandardHelpOptions = true, version = "Authentipay 0.1 😃")
public class AuthentipayCommand implements Callable<Integer> {

    @Parameters(index = "0", paramLabel = "method", defaultValue = "get", description = "The HTTP method you want to use: GET/POST/PUT/PATCH/DELETE. (Default is GET)")
    RequestMethod requestMethod;

    @Option(names = {"-b", "--body"}, paramLabel = "body", description = "A string representing the request's body. The -f|--file option overwrite this.")
    String bodyString;

    @Option(names = {"-f", "--file"}, paramLabel = "file", description = "The path of a JSON file containing the request's body.")
    File bodyFile;

    @Override
    public Integer call() throws Exception {
        OkHttpClient client = new OkHttpClient();

        HttpUrl url = HttpUrl.parse(Constants.URL);

        byte[] body = new byte[]{};
        if (bodyFile != null) {
            body = Files.readAllBytes(bodyFile.toPath());
        } else if (bodyString != null) {
            body = bodyString.getBytes();
        }

        Request request = RequestFactory.make(requestMethod, url, body);

        System.out.println("--- SENDING REQUEST ---\n" + getPrettyPrintableRequest(request) + new String(body, StandardCharsets.UTF_8));
        try {
            Response response = client.newCall(request).execute();
            System.out.println("\n--- RESPONSE ---\n" + response.body().string());
        } catch (IOException e) {
            System.err.println("Unable to receive the response: " + e.getMessage());
        }

        return 0;
    }

    private String getPrettyPrintableRequest(Request request) {
        StringBuilder sb = new StringBuilder();
        sb.append(request.method()).append(" ").append(request.url().toString()).append("\n");

        request.headers().forEach(header -> {
           sb.append(header.component1()).append(": ").append(header.component2()).append("\n");
        });

        return sb.toString();
    }
}
