package authentipay;

/**
 * Class that holds useful constant values.
 */
public class Constants {
    public static final String KEY_ID = "signature-test-66289";
    public static final String ALGORITHM_HEADER_VALUE = "rsa-sha256";
    public static final String URL = "https://staging.authservices.satispay.com/wally-services/protocol/tests/signature";
    public static final String PRIVATE_KEY_FILENAME = "client-rsa-private-key.pem";
    public static final String SIGNATURE_ALGORITHM = "SHA256withRSA";
}
