package authentipay.http;

/**
 * Enum that represent a HTTP method.
 */
public enum RequestMethod {
    GET,
    POST,
	PUT,
	PATCH,
    DELETE;

	@Override
    public String toString() {
        switch (this) {
			case DELETE:
				return "DELETE";
			case GET:
				return "GET";
			case POST:
				return "POST";
			case PUT:
				return "PUT";
			case PATCH:
				return "PATCH";
        }
		return null;
    }
}
