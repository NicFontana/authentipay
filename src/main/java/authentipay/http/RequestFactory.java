package authentipay.http;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

import authentipay.Constants;
import authentipay.signature.RSASigner;
import authentipay.signature.SignatureStringFactory;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.internal.http2.Header;

public class RequestFactory {

    public static Request make(RequestMethod requestMethod, HttpUrl url, byte[] body) {
        ArrayList<Header> signatureHeaders = createSignatureHeaderList(requestMethod, url, body);

        Request.Builder requestBuilder = new Request.Builder().url(url);
        signatureHeaders.forEach((header) -> {
            requestBuilder.addHeader(header.name.utf8(), header.value.utf8());
        });

        Header authorizationHeader = createAuthorizationHeader(requestMethod, url, signatureHeaders);
        requestBuilder.addHeader(authorizationHeader.name.utf8(), authorizationHeader.value.utf8());

        switch (requestMethod) {
            case POST:
                requestBuilder.post(RequestBody.create(body, MediaType.parse("application/json; charset=utf-8")));
                break;
            case PUT:
                requestBuilder.put(RequestBody.create(body, MediaType.parse("application/json; charset=utf-8")));
                break;
            case PATCH:
                requestBuilder.patch(RequestBody.create(body, MediaType.parse("application/json; charset=utf-8")));
                break;
            case DELETE:
                requestBuilder.delete(RequestBody.create(body, MediaType.parse("application/json; charset=utf-8")));
                break;
            default:
                break;
        }

        return requestBuilder.build();
    }

    private static ArrayList<Header> createSignatureHeaderList(RequestMethod requestMethod, HttpUrl url, byte[] body) {
        ArrayList<Header> headers = new ArrayList<Header>();

        headers.add(new Header("Host", url.host()));

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss O");
        String date = formatter.format(ZonedDateTime.now(ZoneOffset.UTC));
        headers.add(new Header("Date", date));

        // Cannot include body in a GET request
        if (requestMethod != RequestMethod.GET) {
            String digest = null;
            try {
                MessageDigest md = MessageDigest.getInstance("SHA-256");
                digest = Base64.getEncoder().encodeToString(md.digest(body));
                headers.add(new Header("Digest", "SHA-256=" + digest));
            } catch (NoSuchAlgorithmException e) {
                System.err.println("Skipping HTTP Digest header calculation: " + e.getMessage());
            }
        }

        return headers;
    }

    private static Header createAuthorizationHeader(RequestMethod requestMethod, HttpUrl url, List<Header> signatureHeaders) {
        RSASigner rsaSigner = RSASigner.newBuilder()
            .setPrivateKeyFilename(Constants.PRIVATE_KEY_FILENAME)
            .setSignatureAlgorithm(Constants.SIGNATURE_ALGORITHM)
            .build();

        String signatureString = SignatureStringFactory.make(requestMethod, url, signatureHeaders);
        byte[] signedSignatureString = rsaSigner.sign(signatureString.getBytes(StandardCharsets.UTF_8));
        String signature = Base64.getEncoder().encodeToString(signedSignatureString);

        List<String> lowercaseHeaderNames = signatureHeaders.stream().map((it) -> { return it.name.utf8().toLowerCase(); }).collect(Collectors.toList());

        StringBuilder sb = new StringBuilder();
        sb.append("Signature keyId=\"").append(Constants.KEY_ID)
            .append("\",algorithm=\"").append(Constants.ALGORITHM_HEADER_VALUE)
            .append("\",headers=\"").append("(request-target) " + String.join(" ", lowercaseHeaderNames))
            .append("\",signature=\"").append(signature)
            .append("\"");

        return new Header("Authorization", sb.toString());
    }    
}
