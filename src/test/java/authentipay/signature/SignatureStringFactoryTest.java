package authentipay.signature;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import authentipay.Constants;
import authentipay.http.RequestMethod;
import okhttp3.HttpUrl;
import okhttp3.internal.http2.Header;

public class SignatureStringFactoryTest {

    @Test void makeACorrectSignatureString() {
        RequestMethod requestMethod = RequestMethod.POST;
        HttpUrl url = HttpUrl.parse(Constants.URL);
        ArrayList<Header> signatureHeaders = new ArrayList<Header>();
        signatureHeaders.add(new Header("Host", "staging.authservices.satispay.com"));   
        signatureHeaders.add(new Header("Date", "Mon, 18 Mar 2019 15:10:24 +0000"));
        signatureHeaders.add(new Header("Digest", "SHA-256=ZML76UQPYzw5yDTmhySnU1S8nmqGde/jhqOG5rpfVSI="));

        String expectedString = "(request-target): post /wally-services/protocol/tests/signature\n" +
        "host: staging.authservices.satispay.com\n" +
        "date: Mon, 18 Mar 2019 15:10:24 +0000\n" +
        "digest: SHA-256=ZML76UQPYzw5yDTmhySnU1S8nmqGde/jhqOG5rpfVSI=";

        assertEquals(expectedString, SignatureStringFactory.make(requestMethod, url, signatureHeaders));

        String expectedStringWithParams = "(request-target): post /wally-services/protocol/tests/signature?foo=bar\n" +
        "host: staging.authservices.satispay.com\n" +
        "date: Mon, 18 Mar 2019 15:10:24 +0000\n" +
        "digest: SHA-256=ZML76UQPYzw5yDTmhySnU1S8nmqGde/jhqOG5rpfVSI=";

        HttpUrl urlWithParam = HttpUrl.parse(Constants.URL + "?foo=bar");
        assertEquals(expectedStringWithParams, SignatureStringFactory.make(requestMethod, urlWithParam, signatureHeaders));
    }

}
