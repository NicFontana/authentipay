package authentipay.signature;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Base64;

import org.junit.jupiter.api.Test;

import authentipay.Constants;

class RSASignerTest {

    private String privateKeyString =
    "-----BEGIN PRIVATE KEY-----" +
    "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQC6DAERacIFrEew" +
    "Lftxai5kTARygaGz/AiZJ3cXRhMxccCtnHeZRx0Hjf1SOc7nXj6+0RNJnXyTwE39" +
    "6gzuQRoN2186kqUSHV38cs79xeEAwMuEQiL9PcGZHbgFqQvsBrq0WX6TKkApcX87" +
    "8mVqYEjb1tYhBqQ1Tx01QpCar99QzHMK/rOJwc3wHIrqVqEjI0oGN55heoKzJxpg" +
    "n7p/XSa2tpZcNI4HLe16sj9vzkuyvV/GgTom1np7TRfqN9fQ+s4yJP4B4D6hTOvM" +
    "/7M3e87JBQwuvc2Bw/fkGX5nqmgOxauIDOd46vsnPHBCneD6kodxdVKzUMTvcV5N" +
    "dutY2otlAgMBAAECggEBALFvF2mrCl7ssqYlBIRi9wGyuh8W4MhN20ltqReqPk3O" +
    "pistaabmepok4e8UsO5aJM9JiPKRXyluozNBj7l+n0GN8aFkEEhZd7R3LlA6818L" +
    "gE2P4dzRlBzaihHj6jndJR0xqRTjm7OqyeS7RduRHouDtk5jDiLFZB7ScCUHDJz5" +
    "7zGghoTcZJFS8rnOXY8loB0yn2+gM82h8DDrg4ItWB/FdQPEKYAOXF9ib1bNM/Gx" +
    "G+QLo2/5IvmkHmbnaqMs8I69Ap5HRhRZJRaJWI8YqRmS2JN/7hFlyMn+VmttB8g3" +
    "74L3nzr+zAUl6OX5b1x5NyjY18i9mJf7pHvkoyEfKkECgYEA41+ThSvlE8gj4MCI" +
    "JTjy/Oh6My5GTS979sFO4oLxtbgRSPkDeMiFdjf97iOBhz1H8X+2oumP9E+3wjdA" +
    "PfGexB4aWJhatblN8NgKuVT/2ujaHeCTplzFRf/5PVOHQp1na3CcFpXCNOE4pVf9" +
    "mOIKN7O+rpO5+LHsUZK3GaMDSbECgYEA0XhxhmE5qMEVKLRDzHPcM3cCT8kpNcXm" +
    "Ef6cJsc7cQGrjal4jLCVWmPp9y1XcS7x9QpFBUMt5XCGBTdp90mDH2PbuBRmI5Tq" +
    "c0ntPBIx/32KfUz6mSqOv2RMrw1Mr19B90GcS9QbafdzC/h7GC3tNHVrHKXxL82F" +
    "6ob/kf0hlfUCgYEAwJpmoSlUrFQeKDWPXlCGbLwVP6OUQ6/Uh2qqgu+/Blq8sZ7W" +
    "VQsvGrxFauNCxqefeK/hqtDEc4TPuUIKNi6leaWyVZgBRuyIXFr1gpbBANO8aBCj" +
    "ogn1xd0WaN+HtWMWhwll/y+uyhJ6ZH1LwaTAWPz2qnVS1JsK/vKUDbBriZECgYBN" +
    "P9bWEPr7oiFUfo5WUxANJsGCfRQmkZIUZspdfrIMLep/dtVPRTv/NsOs9Vq/EeoH" +
    "TT9A/pJpgALc35/Do6eopuH71AIK8zs3QzcrJSatKzYsmXv9inVUXf/tusDiGAYy" +
    "0k56pIFrpecWrg9vTlihNQBIc2YsE+ZkJF8SDsEZFQKBgQCV64s4SQzCpaSWd9xw" +
    "l5QNjIGb8VDqOu3qAZt7EutcYDanGz92MP8yN8Odd42GP9CyL1JhhKd6gDgFJZvs" +
    "zTw84nzgqfv6hEuea/Yz2e9X84Hxgbff0IdduSvcgx33wOzfb5ldx+JU1Pxt8ske" +
    "WT8Iv4/3IFiWmr9O1vllbpI9Rg==" +
    "-----END PRIVATE KEY-----";

    @Test void generateCorrectSignature() {
        String signatureString = "(request-target): post /wally-services/protocol/tests/signature\n" +
        "host: staging.authservices.satispay.com\n" +
        "date: Mon, 18 Mar 2019 15:10:24 +0000\n" +
        "digest: SHA-256=ZML76UQPYzw5yDTmhySnU1S8nmqGde/jhqOG5rpfVSI=";

        RSASigner rsaSigner = RSASigner.newBuilder()
            .setPrivateKeyBytes(privateKeyString.getBytes())
            .setSignatureAlgorithm(Constants.SIGNATURE_ALGORITHM)
            .build();
        
        byte[] signedSignatureString = rsaSigner.sign(signatureString.getBytes());
        String signature = Base64.getEncoder().encodeToString(signedSignatureString);

        // https://satispay-signature-test.glitch.me/signature
        String expectedSignature = "KsprPOuv4HUv+ytGVdKKQezcQ9ocTlnHbDxzv2CnQyxL7SVaup/3ii07ib9oJdUp1bdnWk5O+e9in6fbDUcwK5PNLrErIsCzaIEiAOvS0p3yeLzxgvblNXdCRecOzMXc69JuGOPZLzxyi7aweEAFSCDq57l0dCPbsA0pAMtkPSbiR5rHx+yNNrbQ9fadg0p2wIaJYUwTpIba5q0pelJMGLo8dgGWL39mjQdvZoaMlpYxtUZxy8+EVOuZ4pJKzk2VXNToL5LlofMd86pmUIMkDE2Ueorr0DIjARDMqQpuN8lBurAt2fOfbJC7+6ZI9YWYGztpWaZw+N6PqHkImcz3RA==";

        assertEquals(expectedSignature, signature);
    }
}
